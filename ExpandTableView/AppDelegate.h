//
//  AppDelegate.h
//  ExpandTableView
//
//  Created by Rama kuppa on 12/03/17.
//  Copyright © 2017 sample. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

