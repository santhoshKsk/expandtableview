//
//  ViewController.h
//  ExpandTableView
//
//  Created by Rama kuppa on 12/03/17.
//  Copyright © 2017 sample. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *expandTableView;

@end

