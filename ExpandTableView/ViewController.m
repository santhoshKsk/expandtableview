//
//  ViewController.m
//  ExpandTableView
//
//  Created by Rama kuppa on 12/03/17.
//  Copyright © 2017 sample. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *mainCategory;
    NSMutableArray *subCategory;
    NSMutableArray *expandStatusArray;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Intialize the main Category
    
    self.expandTableView.sectionFooterHeight = 0;
    
    mainCategory = [[NSMutableArray alloc] initWithObjects:@"MEN",@"WOMEN",@"KIDS",@"RECHARGE", nil];
    
    subCategory = [[NSMutableArray alloc] initWithObjects:@[@"Cloths",@"Watches",@"Footwear"],@[@"Cloths",@"Jewellery",@"Footwear",@"Bangles"],@[@"Cloths",@"Toys"],@[@"Jio",@"Bsnl",@"Airtel",@"Uninor",@"Idea",@"vodafone"], nil];
    
    expandStatusArray = [[NSMutableArray alloc] initWithObjects:@"1",@"0",@"0",@"0",nil];
    //@YES for expand the cell
    //@NO for close the cell
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return mainCategory.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    NSString * status = [expandStatusArray objectAtIndex:section];
 
    if ([status isEqualToString:@"1"]) {
        NSArray *subArray = [subCategory objectAtIndex:section];
        return subArray.count;

    }
    return 0;
    
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    NSArray *subArray = [subCategory objectAtIndex:indexPath.section];
    NSString *dataStr = [subArray objectAtIndex:indexPath.row];
    cell.textLabel.text = dataStr;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //Programatically create the view
    CGRect viewFrame = CGRectMake(0, 0, tableView.frame.size.width, 50);
    UIView *headerView = [[UIView alloc] initWithFrame:viewFrame];
    headerView.backgroundColor = [UIColor colorWithRed:76.0f/255.0f green:139.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
    
    //Crate label programatically
    
    UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    headerLbl.text = [mainCategory objectAtIndex:section];
    headerLbl.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:headerLbl];
    
    //Crate Button Programatically
    
    UIButton *expandBtn = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width-50, 0, 50, 50)];
    expandBtn.titleLabel.text = @"+";
    expandBtn.backgroundColor = [UIColor redColor];
    [headerView addSubview:expandBtn];
    expandBtn.tag = section;
    
    [expandBtn addTarget:self action:@selector(expandCell:) forControlEvents:UIControlEventTouchUpInside];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (void)expandCell:(UIButton*)button{
    
    NSString * status = [expandStatusArray objectAtIndex:button.tag];
    
    if ([status isEqualToString:@"1"]) {
        
        //Replace with 0
        [expandStatusArray replaceObjectAtIndex:button.tag withObject:@"0"];
    }else{
        //Replace with 1
        [expandStatusArray replaceObjectAtIndex:button.tag withObject:@"1"];
    }
    [_expandTableView reloadData];

}


@end
